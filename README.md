# Go PS1

A lightning fast bash/zsh prompt written in go

![ps1](go-ps1.png)

# Install

Build `go-ps1` and put it anywhere in your $PATH, then set it up in your shell
config:

## bash

Add the following line to your `.bashrc`:

```
PROMPT_COMMAND='export PS1="$(go-ps1 "$?" "$USER")"'
```

If you have an existing `$PROMPT_COMMAND` you want to keep, you can write a
function that runs multiple commands and set that as your `$PROMPT_COMMAND`.

Here's my config for example:

```
function __prompt_command {
  # Update prompt.
  PS1="$(go-ps1 "$?" "$USER")"

  # Append last command to history.
  history -a

  # This escape sequence sets the X window title.
  if [ -n "$SSH_CONNECTION" ]; then
    printf '\033]0;%s\007' "${HOSTNAME:0:15}:${PWD//${HOME}/\~}"
  else
    printf '\033]0;%s\007' "${PWD//${HOME}/\~}"
  fi
}

PROMPT_COMMAND='__prompt_command'
```

## zsh

Add the following lines to your `.zshrc`:

```
setopt PROMPT_SUBST
PS1='$(ZSH=1 go-ps1 "$?" "$USER")'
```
