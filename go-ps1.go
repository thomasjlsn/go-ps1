/*
 * Generate a PS1 string for bash/zsh prompt.
 * ====================================================================== */

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"strconv"
	"strings"

	"golang.org/x/sys/unix"
)

/* environment variables ================================================ */

var GOPATH = os.Getenv("GOPATH")
var HOME = os.Getenv("HOME")
var SSH = os.Getenv("SSH_CONNECTION")
var TERM = os.Getenv("TERM")
var VENV = os.Getenv("VIRTUAL_ENV")
var ZSH = os.Getenv("ZSH")

/* helper functions ===================================================== */

// Format a string with a PS1-friendly escape code.
func escape(code string, s string) string {
	if s == "" {
		return s
	}
	if ZSH != "" { // zsh
		return fmt.Sprintf(`%%{[%sm%%}%s%%{[0m%%}`, code, s)
	} else { // bash
		return fmt.Sprintf(`\[[%sm\]%s\[[0m\]`, code, s)
	}
}

// Remove empty items from an array/slice.
func removeEmptyItems(s []string) []string {
	var r []string
	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}
	return r
}

// Whether or not a path exists.
func pathExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}

/* PS1 components ======================================================= */

// Return value of previous command, this needs to be passed in as an arg
// because this information can not be ascertained outside of the interactive
// shell session.
func returnValue() string {
	rvString := os.Args[1]
	rvInt, err := strconv.Atoi(rvString)
	if err != nil {
		log.Fatal(err)
	}
	if rvInt == 0 {
		return ""
	} else {
		return string(rvString)
	}
}

// Current username, this needs to be passed in as an arg because if you `su`
// to another user os/user.Username still thinks we're the original user.
func username() string {
	user := os.Args[2]
	return user
}

// Shows where we our SSH session is logged is from.
// TODO? look up hostname of client in /etc/hosts
func sshClient() string {
	if SSH == "" {
		return ""
	}
	sep := "→"
	if TERM == "linux" { // Unicode is a no-go in the console
		sep = "->"
	}
	if strings.HasPrefix(SSH, "::") {
		return fmt.Sprintf("localhost %s", sep)
	} else {
		return fmt.Sprintf("%s %s", strings.Split(SSH, " ")[0], sep)
	}
}

// Hostname, but only if we are in an ssh session.
func hostname(ssh string) string {
	if ssh == "" {
		return ""
	}
	host, err := os.Hostname()
	if err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("@%s", host)
}

// An abbreviated version of $PWD.
func pwdAbbreviate(d string) string {
	re := regexp.MustCompile(`(\.?[^/.])[^/]*/`)
	s := re.ReplaceAllString(
		strings.Replace(d, HOME, "~", 1),
		`$1/`,
	)
	return s
}

// Whether or not $PWD is read only.
func readOnly(d string) string {
	access := unix.Access(d, unix.W_OK)
	if access != nil {
		ro := "🔒"
		if TERM == "linux" { // Unicode is a no-go in the console
			ro = "[RO] "
		}
		return ro
	}
	return ""
}

// Name of current git branch.
func gitBranch(d string) string {
	if d == "/" {
		return ""
	}

	gitDir := filepath.Join(d, ".git")

	if pathExists(gitDir) {
		var branch string
		var sha string

		HeadFile := filepath.Join(gitDir, "HEAD")
		HeadFileContents, _ := ioutil.ReadFile(HeadFile)
		HEAD := string(HeadFileContents)

		if strings.Contains(HEAD, ":") {
			refLoc := strings.TrimSpace(strings.Split(HEAD, ":")[1])
			refLocAbs := filepath.Join(gitDir, refLoc)
			branch = filepath.Base(refLoc)

			if pathExists(refLocAbs) {
				shaRaw, _ := ioutil.ReadFile(refLocAbs)
				sha = strings.TrimSpace(string(shaRaw))[0:8]
			} else {
				sha = "no commits"
			}
		} else {
			sha = HEAD[0:8]
			branch = "detached HEAD"
		}

		return fmt.Sprintf("[%s:%s]", branch, sha)
	} else {
		return gitBranch(filepath.Dir(d))
	}
}

// Shows go version if we are in a subdirectory of $GOPATH
func goVersion(d string) string {
	if GOPATH == "" {
		return ""
	}
	if strings.HasPrefix(d, GOPATH) {
		return fmt.Sprintf("[go:%s]", runtime.Version()[2:])
	}
	return ""
}

// Name of python virtual environment.
func virtualEnv() string {
	if VENV == "" {
		return ""
	} else {
		return fmt.Sprintf("[venv:%s]", filepath.Base(VENV))
	}
}

// Insert a line break if some string exists.
func lineBreak(s string) string {
	if s == "" {
		return ""
	} else {
		return "\n"
	}
}

/* ====================================================================== */

// main is used for configuring/building the prompt.
func main() {
	pwd, err := os.Getwd()

	if err != nil {
		// $PWD has been deleted, in which case the other information in the
		// prompt is irrelevant until $USER remedies this situation. This
		// (ab)uses $PS1 as a means to display an error message to $USER.
		fmt.Printf("%s %s ", escape("31;1", "[$PWD does not exist]"), escape("1", "!!"))
		os.Exit(0)
	}

	// Prompt character for bash.
	promptChar := "$"

	// Prompt character for zsh.
	if ZSH != "" {
		// % is an special character in zsh's PS1, %% escapes it.
		promptChar = "%%"
	}

	// Information about our environment.
	envInfo := strings.Join(
		[]string{
			escape("1;31", readOnly(pwd)),
			escape("1;35", gitBranch(pwd)),
			escape("1;35", virtualEnv()),
			escape("1;36", goVersion(pwd)),
		},
		"",
	)

	retval := escape("1;31", returnValue())
	ssh := escape("1;33", sshClient())
	user := escape("1;32", username())
	host := escape("1", hostname(ssh)) // replace ssh with a non-empty string to always show hostname
	pwd = escape("2", pwdAbbreviate(pwd))
	promptChar = escape("1", promptChar)

	promptConfig := []string{
		retval,
		ssh,
		user + host,
		pwd,
		envInfo,
		lineBreak(ssh+envInfo) + promptChar,
	}

	PS1 := strings.Join(removeEmptyItems(promptConfig[:]), " ")

	fmt.Printf("%s ", PS1)
}
