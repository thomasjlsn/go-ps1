build:
	go build -v -x -ldflags "-s -w" go-ps1.go

install:
	go install -v -x -ldflags "-s -w" go-ps1.go

clean:
	go clean -x

go.mod:
	go mod init ps1

dependencies: go.mod
	go get golang.org/x/sys/unix
